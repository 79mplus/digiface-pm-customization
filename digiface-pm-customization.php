<?php
/**
Plugin Name: DigiFace Project Manager Customization
Description: Customisation done on WP Project Manager
Plugin URI: http://www.digiface.org/
Author: weDevs
Author URI: https://wedevs.com
Version: 1.0
Text Domain: digiface-pm-customization
Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * The main plugin class
 */
final class Digiface_PM_Customization{
    /**
     * Instance of self
     *
     * @var Digiface_PM_Customization
     */
    private static $instance = null;

    /**
     * Class constructor
     */
    private function __construct() {

        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
        add_filter( 'pm_localize', array( $this, 'pm_localize' ) );
        add_filter( 'user_has_cap', [ $this, 'add_user_project_create_capability' ] );
        add_action( 'pm_after_update_project', [ $this, 'update_project_visibility_setting' ], 10, 2);
        add_action( 'pm_after_update_project', [ $this, 'update_invoice_currency' ], 10, 2);
        add_filter( 'pm_project_transform', [ $this, 'append_visiblity_info_to_project_data' ], 10, 2);
        add_filter( 'pm_project_where_query', [ $this, 'modify_project_query' ] );
        add_filter( 'pre_get_avatar_data', [ $this, 'update_avatar_url' ], 10, 2 );
        add_filter( 'get_avatar_data', [ $this, 'update_avatar_url' ], 99, 2 );
        add_action( 'show_user_profile', [ $this, 'profile' ] );
        add_action( 'personal_options_update', [ $this, 'profile_update' ] );

        register_activation_hook( __FILE__, [ $this, 'activate' ] );
        register_deactivation_hook( __FILE__, [ $this, 'deactivate' ] );
    }

    /**
     * Initializes the Digiface_PM_Customization() class
     *
     * Checks for an existing Digiface_PM_Customization() instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {

        if ( self::$instance === null ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /*
     * enqueue scripts
     * */
    public function enqueue_scripts(){
       wp_enqueue_script( 'pm-customization', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'assets/js/pm-customization.js', ['jquery'] );
       wp_enqueue_style( 'pm-customization', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'assets/css/pm-customization.css' );
    }

    public function pm_localize( $loc ){
        $loc['current_user']->caps['pm_manager'] = true;
        if(function_exists('get_field')){
            $profile_picture = get_field( 'profile_image', 'user_' . $loc['current_user']->ID);
            if($profile_picture){
                $loc['avatar_url'] = $profile_picture['url'];
            }
        }
        return $loc;
    }

    /**
     * allow any user to create project
     * @param $caps
     * @return mixed
     */
    public function add_user_project_create_capability( $caps ){
        if(preg_match('/\/wp-json\/pm\/v2\/projects\/$/', $_SERVER[ 'REQUEST_URI' ] ) && strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
            $caps[pm_manager_cap_slug()] = true;
        }
        return $caps;
    }

    /**
     * update the visibility of a project
     * @param $resp
     * @param $params
     */
    public function update_project_visibility_setting( $resp, $params ){
        if( isset( $params['id'] ) && isset( $params['public'] ) ){
            pm_update_meta( 0, $params['id'], 'visiblity_settings', 'visible_to_public', $params['public'] );
        }
    }
    
    /**
     * update the invoice currency of a project
     * @param $resp
     * @param $params
     */
    public function update_invoice_currency( $resp, $params ){
        if( isset( $params['id'] ) && isset( $params['invoice_currency'] ) ){
            pm_update_meta( 0, $params['id'], 'invoice_currency', 'invoice_currency', $params['invoice_currency'] );
        }
    }

    /**
     * @param $items
     * @param $project
     * @return mixed
     */
    public function append_visiblity_info_to_project_data($items, $project){
        $meta = WeDevs\PM\Common\Models\Meta::where('project_id', (int)$project->id)->where('meta_key', 'visible_to_public');
        $items['visible_to_public'] = $meta->first()? $meta->first()->meta_value : 'no';
        /* also append the invoice currency setting */
        $meta = WeDevs\PM\Common\Models\Meta::where('project_id', (int)$project->id)->where('meta_key', 'invoice_currency');
        $items['invoice_currency'] = $meta->first()? $meta->first()->meta_value : '';
        return $items;
    }

    /**
     * modify project query so that all public projects are returned
     * @param $where
     * @return string
     */
    public function modify_project_query($where){
        global $wpdb;

        $where .= " OR {$wpdb->prefix}pm_projects.id IN (SELECT project_id from {$wpdb->prefix}pm_meta WHERE meta_key = 'visible_to_public' AND meta_value = 'yes')";
        return $where;
    }

    public function update_avatar_url($args, $id_or_email){
        if(isset($args['url']) && strpos($args['url'], 'profile_url=') !== false) return $args;
        if(is_numeric($id_or_email)){
            $user_id = $id_or_email;
        }else{
            $user = get_user_by('email', $id_or_email);
            $user_id = $user->ID;
        }
        if(function_exists('get_field')){
            $profile_picture = get_field( 'profile_image', 'user_' . $user_id);
            if($profile_picture){
                $args['url'] = $profile_picture['url'];
            }
        }
        if(isset($args['url'])){
            $query = parse_url($args['url'], PHP_URL_QUERY);
            if ($query) {
                $args['url'] .= '&profile_url=' .  get_author_posts_url($user_id);
            } else {
                $args['url'] .= '?profile_url=' .  get_author_posts_url($user_id);
            }
        }

        return $args;
    }

    /**
     * add the WP Project Manager notfication settings in profile
     * 
     * @param WP_User $profile_user User data
     */

    public function profile($profile_user){ 
        if( $profile_user->ID == get_current_user_id() && ! current_user_can( 'manage_options' ) ){
            ?>
            <h3><?php esc_html_e( 'WP Project Manager', 'wedevs-project-manager' ); ?></h3>
            <?php
            do_action( 'pm_user_profile', $profile_user );
        }
    }

    public function profile_update($user_id){
        if( $user_id == get_current_user_id() ){
            /* saving the data */
            $daily_digest_active_status                      = isset( $_POST['pm_daily_digets_status'] ) ? $_POST['pm_daily_digets_status'] : 'off';
            $email_notification_active_status                = isset( $_POST['pm_email_notification'] ) ? $_POST['pm_email_notification'] : 'off';

            $email_notification_new_project_active_status    = isset( $_POST['pm_email_notification_new_project'] ) ? $_POST['pm_email_notification_new_project'] : 'off';
            $email_notification_update_project_active_status = isset( $_POST['pm_email_notification_update_project'] ) ? $_POST['pm_email_notification_update_project'] : 'off';
            $email_notification_new_message_active_status    = isset( $_POST['pm_email_notification_new_message'] ) ? $_POST['pm_email_notification_new_message'] : 'off';
            $email_notification_new_comment_active_status    = isset( $_POST['pm_email_notification_new_comment'] ) ? $_POST['pm_email_notification_new_comment'] : 'off';
            $email_notification_update_comment_active_status = isset( $_POST['pm_email_notification_update_comment'] ) ? $_POST['pm_email_notification_update_comment'] : 'off';
            $email_notification_new_task_active_status       = isset( $_POST['pm_email_notification_new_task'] ) ? $_POST['pm_email_notification_new_task'] : 'off';
            $email_notification_update_task_active_status    = isset( $_POST['pm_email_notification_update_task'] ) ? $_POST['pm_email_notification_update_task'] : 'off';
            $email_notification_complete_task_active_status  = isset( $_POST['pm_email_notification_complete_task'] ) ? $_POST['pm_email_notification_complete_task'] : 'off';

            if ( is_user_logged_in() ) {
                update_user_meta( $user_id, '_user_daily_digets_status', $daily_digest_active_status );
                update_user_meta( $user_id, '_cpm_email_notification', $email_notification_active_status );

                update_user_meta( $user_id, '_cpm_email_notification_new_project', $email_notification_new_project_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_update_project', $email_notification_update_project_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_new_message', $email_notification_new_message_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_new_comment', $email_notification_new_comment_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_update_comment', $email_notification_update_comment_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_new_task', $email_notification_new_task_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_update_task', $email_notification_update_task_active_status );
                update_user_meta( $user_id, '_cpm_email_notification_complete_task', $email_notification_complete_task_active_status );

            }
        }
    }

    /**
     * Define the required plugin constants
     *
     * @return void
     */
    public function define_constants() {

    }


    /**
     * Placeholder for activation function
     *
     * Nothing being called here yet.
     *
     * @return void
     */
    public function activate() {

    }

    /**
     * Placeholder for deactivation function
     *
     * Nothing being called here yet.
     *
     * @return void
     */
    public function deactivate() {

    }
}

Digiface_PM_Customization::init();