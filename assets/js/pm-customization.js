const project_url_pattern = /#\/projects\/(?<id>[0-9]+)\//;

const pm_hook_checker = setInterval(()=>{
    if(typeof WeDevsfilters != 'undefined'){
        clearInterval(pm_hook_checker);
        if(typeof WeDevsfilters.before_project_save == 'undefined'){
            WeDevsfilters.before_project_save = [];
        }
        WeDevsfilters.before_project_save.push({
            priority: 10,
            callback: (value, options) => {
                if(typeof value.data.id != 'undefined'){
                    value.data.public = jQuery("#project_visibility_" + value.data.id).is(":checked") ? 'yes' : 'no';
                    value.data.invoice_currency = jQuery("select[name='invoice_currency']").val();
                    if(value.data.invoice_currency != ''){
                      PM_Vars.settings.invoice.currency_code = value.data.invoice_currency;
                    }
                }
                return value;
            }
        });

    }
}, 2000);
// Callback function to execute when mutations are observed
const observe_pm_callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(const mutation of mutationsList) {
        if (mutation.type === 'childList') {
            if(mutation.previousSibling != null){
                let sibling = jQuery(mutation.previousSibling);
                if(sibling.hasClass('project-edit-form')){
                    let match = document.location.hash.match(project_url_pattern);
                    if(match == null) return;
                    let groups = match.groups;
                    let checked = '';
                    let invoice_currency = PM_Vars.settings.invoice.currency_code;
                    for(var i = 0; i < pmProjects.length; i++){
                        if(pmProjects[i].project_id == groups.id && pmProjects[i].visible_to_public == 'yes'){
                            checked = " checked ";
                        }
                        if(pmProjects[i].project_id == groups.id ){
                          if( typeof pmProjects[i].invoice_currency != undefined && pmProjects[i].invoice_currency ){
                             invoice_currency = pmProjects[i].invoice_currency;
                          }
                      }

                    }
                    let visibility_checkbox = `
							<div class="pm-form-item item project-notify">
									<label>
										<input type="checkbox" name="project_visibility" id="project_visibility_${groups.id}" value="yes" ${checked} style="margin-right: 5px;">
										Visible to All
									</label>
							</div>`;
                    let currency_select =  `
                    <div class="pm-form-item item project-notify">
                            <label>
                                Invoice Currency
                                <select style='width: 50%;' name="invoice_currency">
                                    ${currencies.map(c => '<option value="' + c[0] + '" '+(invoice_currency == c[0] ? 'selected': '')+'>' + c[1] + '</option>').join('')}
                                </select>
                            </label>
                    </div>`;       
                    sibling.find('.submit').before(visibility_checkbox + currency_select);
                }
            }
        }
    }
};
const observe_pm = new MutationObserver(observe_pm_callback);
const pm_div_checker = setInterval(() => {
    if(document.getElementById('wedevs-project-manager') != null){
        clearInterval(pm_div_checker);
        observe_pm.observe(document.getElementById('wedevs-project-manager'), { attributes: false, childList: true, subtree: true });
        /**
         * link the avatar to profile page
         */
        jQuery("#wedevs-project-manager").on('click','img',function(){
            let el = jQuery(this);
            let url_parts = el.attr('src').split('profile_url=');
            if(url_parts.length == 2){
                document.location = url_parts[1]
            }
        });

    }
}, 2000);

observe_pm.disconnect();



var currencies = [
    [
      "USD",
      "US Dollar"
    ],
    [
      "CAD",
      "Canadian Dollar"
    ],
    [
      "EUR",
      "Euro"
    ],
    [
      "AED",
      "United Arab Emirates Dirham"
    ],
    [
      "AFN",
      "Afghan Afghani"
    ],
    [
      "ALL",
      "Albanian Lek"
    ],
    [
      "AMD",
      "Armenian Dram"
    ],
    [
      "ARS",
      "Argentine Peso"
    ],
    [
      "AUD",
      "Australian Dollar"
    ],
    [
      "AZN",
      "Azerbaijani Manat"
    ],
    [
      "BAM",
      "Bosnia-Herzegovina Convertible Mark"
    ],
    [
      "BDT",
      "Bangladeshi Taka"
    ],
    [
      "BGN",
      "Bulgarian Lev"
    ],
    [
      "BHD",
      "Bahraini Dinar"
    ],
    [
      "BIF",
      "Burundian Franc"
    ],
    [
      "BND",
      "Brunei Dollar"
    ],
    [
      "BOB",
      "Bolivian Boliviano"
    ],
    [
      "BRL",
      "Brazilian Real"
    ],
    [
      "BWP",
      "Botswanan Pula"
    ],
    [
      "BYR",
      "Belarusian Ruble"
    ],
    [
      "BZD",
      "Belize Dollar"
    ],
    [
      "CDF",
      "Congolese Franc"
    ],
    [
      "CHF",
      "Swiss Franc"
    ],
    [
      "CLP",
      "Chilean Peso"
    ],
    [
      "CNY",
      "Chinese Yuan"
    ],
    [
      "COP",
      "Colombian Peso"
    ],
    [
      "CRC",
      "Costa Rican Colón"
    ],
    [
      "CVE",
      "Cape Verdean Escudo"
    ],
    [
      "CZK",
      "Czech Republic Koruna"
    ],
    [
      "DJF",
      "Djiboutian Franc"
    ],
    [
      "DKK",
      "Danish Krone"
    ],
    [
      "DOP",
      "Dominican Peso"
    ],
    [
      "DZD",
      "Algerian Dinar"
    ],
    [
      "EEK",
      "Estonian Kroon"
    ],
    [
      "EGP",
      "Egyptian Pound"
    ],
    [
      "ERN",
      "Eritrean Nakfa"
    ],
    [
      "ETB",
      "Ethiopian Birr"
    ],
    [
      "GBP",
      "British Pound Sterling"
    ],
    [
      "GEL",
      "Georgian Lari"
    ],
    [
      "GHS",
      "Ghanaian Cedi"
    ],
    [
      "GNF",
      "Guinean Franc"
    ],
    [
      "GTQ",
      "Guatemalan Quetzal"
    ],
    [
      "HKD",
      "Hong Kong Dollar"
    ],
    [
      "HNL",
      "Honduran Lempira"
    ],
    [
      "HRK",
      "Croatian Kuna"
    ],
    [
      "HUF",
      "Hungarian Forint"
    ],
    [
      "IDR",
      "Indonesian Rupiah"
    ],
    [
      "ILS",
      "Israeli New Sheqel"
    ],
    [
      "INR",
      "Indian Rupee"
    ],
    [
      "IQD",
      "Iraqi Dinar"
    ],
    [
      "IRR",
      "Iranian Rial"
    ],
    [
      "ISK",
      "Icelandic Króna"
    ],
    [
      "JMD",
      "Jamaican Dollar"
    ],
    [
      "JOD",
      "Jordanian Dinar"
    ],
    [
      "JPY",
      "Japanese Yen"
    ],
    [
      "KES",
      "Kenyan Shilling"
    ],
    [
      "KHR",
      "Cambodian Riel"
    ],
    [
      "KMF",
      "Comorian Franc"
    ],
    [
      "KRW",
      "South Korean Won"
    ],
    [
      "KWD",
      "Kuwaiti Dinar"
    ],
    [
      "KZT",
      "Kazakhstani Tenge"
    ],
    [
      "LBP",
      "Lebanese Pound"
    ],
    [
      "LKR",
      "Sri Lankan Rupee"
    ],
    [
      "LTL",
      "Lithuanian Litas"
    ],
    [
      "LVL",
      "Latvian Lats"
    ],
    [
      "LYD",
      "Libyan Dinar"
    ],
    [
      "MAD",
      "Moroccan Dirham"
    ],
    [
      "MDL",
      "Moldovan Leu"
    ],
    [
      "MGA",
      "Malagasy Ariary"
    ],
    [
      "MKD",
      "Macedonian Denar"
    ],
    [
      "MMK",
      "Myanma Kyat"
    ],
    [
      "MOP",
      "Macanese Pataca"
    ],
    [
      "MUR",
      "Mauritian Rupee"
    ],
    [
      "MXN",
      "Mexican Peso"
    ],
    [
      "MYR",
      "Malaysian Ringgit"
    ],
    [
      "MZN",
      "Mozambican Metical"
    ],
    [
      "NAD",
      "Namibian Dollar"
    ],
    [
      "NGN",
      "Nigerian Naira"
    ],
    [
      "NIO",
      "Nicaraguan Córdoba"
    ],
    [
      "NOK",
      "Norwegian Krone"
    ],
    [
      "NPR",
      "Nepalese Rupee"
    ],
    [
      "NZD",
      "New Zealand Dollar"
    ],
    [
      "OMR",
      "Omani Rial"
    ],
    [
      "PAB",
      "Panamanian Balboa"
    ],
    [
      "PEN",
      "Peruvian Nuevo Sol"
    ],
    [
      "PHP",
      "Philippine Peso"
    ],
    [
      "PKR",
      "Pakistani Rupee"
    ],
    [
      "PLN",
      "Polish Zloty"
    ],
    [
      "PYG",
      "Paraguayan Guarani"
    ],
    [
      "QAR",
      "Qatari Rial"
    ],
    [
      "RON",
      "Romanian Leu"
    ],
    [
      "RSD",
      "Serbian Dinar"
    ],
    [
      "RUB",
      "Russian Ruble"
    ],
    [
      "RWF",
      "Rwandan Franc"
    ],
    [
      "SAR",
      "Saudi Riyal"
    ],
    [
      "SDG",
      "Sudanese Pound"
    ],
    [
      "SEK",
      "Swedish Krona"
    ],
    [
      "SGD",
      "Singapore Dollar"
    ],
    [
      "SOS",
      "Somali Shilling"
    ],
    [
      "SYP",
      "Syrian Pound"
    ],
    [
      "THB",
      "Thai Baht"
    ],
    [
      "TND",
      "Tunisian Dinar"
    ],
    [
      "TOP",
      "Tongan Paʻanga"
    ],
    [
      "TRY",
      "Turkish Lira"
    ],
    [
      "TTD",
      "Trinidad and Tobago Dollar"
    ],
    [
      "TWD",
      "New Taiwan Dollar"
    ],
    [
      "TZS",
      "Tanzanian Shilling"
    ],
    [
      "UAH",
      "Ukrainian Hryvnia"
    ],
    [
      "UGX",
      "Ugandan Shilling"
    ],
    [
      "UYU",
      "Uruguayan Peso"
    ],
    [
      "UZS",
      "Uzbekistan Som"
    ],
    [
      "VEF",
      "Venezuelan Bolívar"
    ],
    [
      "VND",
      "Vietnamese Dong"
    ],
    [
      "XAF",
      "CFA Franc BEAC"
    ],
    [
      "XOF",
      "CFA Franc BCEAO"
    ],
    [
      "YER",
      "Yemeni Rial"
    ],
    [
      "ZAR",
      "South African Rand"
    ],
    [
      "ZMK",
      "Zambian Kwacha"
    ]
  ];