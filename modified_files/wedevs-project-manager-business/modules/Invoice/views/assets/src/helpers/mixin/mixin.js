import Countries from './../countries/countries';
import Currency from './../currency/currency';

export default {

    data () {
        return {
        	organization: this.getSettings('invoice', ''),
            countries: Countries,
            currency: Currency,
            currencySymbol: '',
            isAdmin: PM_Vars.is_admin == '1' ? true : false,
            can_payment_submit: true,
        }
    },

    created () {
		this.currencySymbol = this.getCurrencySymbol(this.organization.currency_code)
		for(var i = 0; i < pmProjects.length; i++){
			if(pmProjects[i].project_id == this.project_id ){
			  if( typeof pmProjects[i].invoice_currency != undefined && pmProjects[i].invoice_currency ){
				  this.currencySymbol = this.getCurrencySymbol(pmProjects[i].invoice_currency)
			  }  
			}
		}
        
    },

    methods: {
        sendPdfMail (args) {
            var self = this;
            var request_data = {
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id+'/mail/',
                type: 'POST',
                data: {},
                success (res) {
                    if( res === true ) {
                        pm.Toastr.success('E-mail send successfully');

                        if (typeof args.callback != 'undefined') {
                            args.callback(res);
                        }
                    }
                },
            }

            self.httpRequest(request_data);
        },

        generatePDF (invoice) {
            window.location.href = `${this.base_url}pm-pro/v2/projects/${invoice.project_id}/invoice/${invoice.id}/pdf?_wpnonce=${PM_Vars.permission}`;
        },

        getCountryName (code) {
            if (!code) {
                return ;
            }
            let index = this.getIndex(this.countries, code, 'code');

            return this.countries[index].name;
        },

        getCurrencySymbol (code) {
            code = code || 'USD';
            let index = this.getIndex(this.currency, code, 'code');

            return this.currency[index].symbol_native;
        },
    	showHideInvoiceForm (status, invoice) {
            var invoice   = invoice || false,
                invoice   = invoice.id ? invoice : false;

            if ( invoice ) {
                if ( status === 'toggle' ) {
                    invoice.edit_mode = invoice.edit_mode ? false : true;
                } else {
                    invoice.edit_mode = status;
                }
            } else {
                this.$store.commit( 'invoice/showHideInvoiceForm', status);
            }
        },

        saveInvoice (args) {
            var self = this;
            var request_data = {
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice',
                type: 'POST',
                data: args.data,
                success (res) {
                    self.showHideInvoiceForm(false);
                    self.$store.commit( 'invoice/newInvoice', res.data );
                    self.$store.commit( 'invoice/updateMetaAfterNewInvoice' );

                    if (typeof args.callback != 'undefined') {
                        args.callback(res);
                    }
                },
            }

            self.httpRequest(request_data);
        },

        saveOrganizationAddress (args) {
        	var self = this;
        	var request_data = {
                url: self.base_url + 'pm-pro/v2/invoice/address',
                type: 'POST',
                data: args.data,
                success (res) {
                    PM_Vars.settings['invoice'] = res.data.value;

                    if (typeof args.callback != 'undefined') {
                        args.callback(res);
                    }
                },
            }

            self.httpRequest(request_data);
        },

        getUserAddress (args) {
            var self = this;
            var request_data = {
                url: self.base_url + 'pm-pro/v2/invoice/user-address/user/' + args.user_id,
                data: {},
                success (res) {

                    if (typeof args.callback !== 'undefined') {
                        args.callback(res);
                    }
                },
            }

            self.httpRequest(request_data);
        },

        saveUserAddres (args) {
            var self = this;
            var request_data = {
                url: self.base_url + 'pm-pro/v2/invoice/user-address/user/' + args.user_id,
                type: 'POST',
                data: args.data,
                success (res) {

                    if (typeof args.callback !== 'undefined') {
                        args.callback(res);
                    }
                },
            }

            self.httpRequest(request_data);
        },

        getInvoices (args) {
            var self = this;
            var pre_define = {
                    conditions: {
                        per_page: 2,
                        page: 1,
                    },
                    callback: false
                };

            var args       = jQuery.extend(true, pre_define, args );
            var conditions = self.generateConditions(args.conditions);
            if (self.project_id) {
                var url = self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/?'+conditions;
            } else {
                if (!PM_Pro_Vars.project_id) {
                    var url = self.base_url + 'pm-pro/v2/invoices/?'+conditions;
                } else {
                    var url = self.base_url + 'pm-pro/v2/projects/'+PM_Pro_Vars.project_id+'/invoice/?frontend=1&'+conditions;
                }
            }


            var request = {
                url: url,
                success (res) {
                    res.data.map(function(invoice, index) {
                        self.addInvoiceMeta(invoice);
                    });
                    self.$store.commit( 'invoice/setInvoices', res.data );
                    self.$store.commit( 'invoice/setInvoiceMeta', res.meta );

                    if (typeof args.callback === 'function') {
                        args.callback(res.data);
                    }
                }
            };
            self.httpRequest(request);
        },

        addInvoiceMeta (invoice) {
            invoice.edit_mode = false;
            invoice.organizationAddressForm = false;
            invoice.clientAddressForm = false;
            //invoice.isPartialActive = false;
        },

        getStatus (status) {
            var assigeen = {
                0: 'Incomplete',
                1: 'Complete',
                3: 'Partial'
            }

            return assigeen[status];
        },

        updateInvoice (args) {

            var self = this;

            // Disable submit button for preventing multiple click
            this.submit_disabled = true;


            // Showing loading option
            this.show_spinner = true;

            var request_data = {
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id,
                type: 'POST',
                data: args.data,
                success (res) {
                    self.show_spinner = false;
                    // Display a success toast, with a title
                    pm.Toastr.success(res.message);
                    self.addInvoiceMeta(res.data);
                    self.submit_disabled = false;
                    self.showHideInvoiceForm(false);
                    self.$store.commit( 'invoice/updateInvoice', res.data );
                    self.$store.commit( 'invoice/setInvoice', res.data );

                    if (typeof args.callback === 'function') {
                        args.callback(res.data);
                    }
                },

                error (res) {
                    self.show_spinner = false;

                    // Showing error
                    res.data.error.map( function( value, index ) {
                        pm.Toastr.error(value);
                    });
                    self.submit_disabled = false;
                }
            }
            self.httpRequest(request_data);
        },

        getInvoice (args) {
            var self = this;
            var pre_define = {
                    conditions: {

                    },
                    callback: false
                };

            var args       = jQuery.extend(true, pre_define, args );
            var conditions = self.generateConditions(args.conditions);

            var request = {
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id+'?'+conditions, ///with=comments',
                success (res) {
                    self.addInvoiceMeta(res.data);
                    self.$store.commit( 'invoice/setInvoice', res.data );

                    if(typeof args.callback === 'function' ) {
                        args.callback(res.data);
                    }
                }
            };
            self.httpRequest(request);
        },

        deleteInvoice (args) {
            if (!confirm('Are you sure!')) {
                return;
            }
            var self = this;

            var request = {
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id, ///with=comments',
                type: 'DELETE',
                success (res) {
                    self.$store.commit( 'invoice/deleteInvoice', args.invoice_id );

                    if (!self.$store.state.invoice.invoices.length) {
                        self.$router.push({
                            name: 'invoice',
                            params: {
                                project_id: self.project_id
                            }
                        });
                    } else {
                        self.getInvoices();
                    }

                    if( typeof args.callback === 'function' ) {
                        args.callback(res.data);
                    }
                }
            };
            self.httpRequest(request);
        },

        taskLineTotal (task) {
            task.amount = task.amount || 0;
            var amount = parseFloat(task.amount);
            var hour  = parseFloat(task.hour) ? parseFloat(task.hour) : 0;

            // var tax       = parseFloat(task.tax)/100;
            // var taxAmount = amount*hour*tax;
            var lineTotal = parseFloat(amount*hour);

            return lineTotal.toFixed(2);
        },

        nameLineTotal (task) {
            task.amount = task.amount || 0;
            var amount = parseFloat(task.amount);
            var quantity  = parseFloat(task.quantity) ? parseFloat(task.quantity) : 0;

            // var tax       = parseFloat(task.tax)/100;
            // var taxAmount = amount*quantity*tax;
            var lineTotal = parseFloat(amount*quantity);

            return lineTotal.toFixed(2);
        },

        lineTax(task, type) {
            if (type == 'task') {
                task.amount = task.amount || 0;
                var amount = parseFloat(task.amount);
                var hour  = parseFloat(task.hour) ? parseFloat(task.hour) : 0;

                var tax       = parseFloat(task.tax)/100;
                var taxAmount = amount*hour*tax;

                return taxAmount;
            }

            if ( type == 'name') {
                task.amount = task.amount || 0;
                var amount = parseFloat(task.amount);
                var quantity  = parseFloat(task.quantity) ? parseFloat(task.quantity) : 0;

                var tax       = parseFloat(task.tax)/100;
                var taxAmount = amount*quantity*tax;

                return taxAmount;
            }
        },

        lineDiscount(task, discount, type) {
            discount = discount || 0;

            if (type == 'task') {
                task.amount = task.amount || 0;
                var amount = parseFloat(task.amount);
                var hour  = parseFloat(task.hour) ? parseFloat(task.hour) : 0;

                var discount       = parseFloat(discount)/100;
                var discountAmount = amount*hour*discount;

                return discountAmount;
            }

            if ( type == 'name') {
                task.amount = task.amount || 0;
                var amount = parseFloat(task.amount);
                var quantity  = parseFloat(task.quantity) ? parseFloat(task.quantity) : 0;

                var discount = parseFloat(discount)/100;
                var discountAmount = amount*quantity*discount;

                return discountAmount;
            }
        },

        calculateSubTotal (tasks, names) {
            var subTotal = 0;

            tasks.forEach(function(task) {
                task.amount = task.amount || 0;
                var amount = parseFloat(task.amount);
                var hour   = parseFloat(task.hour) ? parseFloat(task.hour) : 0;
                subTotal   = subTotal + (amount*hour);
            });

            names.forEach(function(task) {
                task.amount = task.amount || 0;
                var amount   = parseFloat(task.amount);
                var quantity = parseFloat(task.quantity) ? parseFloat(task.quantity) : 0;
                subTotal     = subTotal + (amount*quantity);
            });

            return parseFloat(subTotal).toFixed(2);
        },

        calculateTotalTax(tasks, names) {
            var self = this;
            var taxTotal = 0;

            tasks.forEach(function(task) {
                let lineTax = self.lineTax(task, 'task');
                    lineTax = lineTax || 0;
                taxTotal = taxTotal + parseFloat(lineTax);
            });

            names.forEach(function(task) {
                let lineTax = self.lineTax(task, 'name');
                    lineTax = lineTax || 0;
                taxTotal = taxTotal + parseFloat(lineTax);
            });

            return taxTotal.toFixed(2);
        },

        calculateTotalDiscount (tasks, names, discount) {
            var self = this;
            var discountTotal = 0;

            tasks.forEach(function(task) {
                let lineDiscount = self.lineDiscount(task, discount, 'task');
                    lineDiscount = lineDiscount || 0;
                discountTotal = discountTotal + parseFloat(lineDiscount);
            });

            names.forEach(function(task) {
                let lineDiscount = self.lineDiscount(task, discount, 'name');
                    lineDiscount = lineDiscount || 0;
                discountTotal = discountTotal + parseFloat(lineDiscount);
            });

            return discountTotal.toFixed(2);
        },

        invoiceTotal (tasks, names, discount) {
            var subTotal = this.calculateSubTotal(tasks, names);
            var totalTax = this.calculateTotalTax(tasks, names);
            var totalDiscount = this.calculateTotalDiscount(tasks, names, discount);

            var total = (parseFloat(subTotal)+parseFloat(totalTax)) - parseFloat(totalDiscount);

            return total.toFixed(2);
        },

        savePayments (args) {
            var self = this;

            var request = {
                data: args.data,
                type: 'POST',
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id+'/payment', ///with=comments',
                success (res) {

                    if(typeof args.callback === 'function' ) {
                        args.callback(res.data);
                    }
                },

                error (res) {
                    self.can_submit = true;

                    // Showing error
                    res.responseJSON.message.forEach( function( value, index ) {
                        pm.Toastr.error(value);
                    });
                }
            };
            self.httpRequest(request);
        },
        totalPaid (payments) {
            payments = payments || [];

            var totalPaid = 0;

            payments.forEach(function(payment) {
                payment.amount = payment.amount || 0;
                totalPaid = totalPaid + parseFloat(payment.amount);
            });

            return totalPaid.toFixed(2);
        },

        dueAmount (invoice) {
            let paid = this.totalPaid(invoice.payments.data);
            let total = this.invoiceTotal(invoice.entryTasks, invoice.entryNames, invoice.discount);
            var dueTotal = parseFloat(total) - parseFloat(paid);

            return dueTotal.toFixed(2);
        },

        sendToPaypal (args) {
            var self = this;
            var listener_url    = PM_Pro_Vars.listener_url,
                return_url      = PM_Pro_Vars.return_url,
                currentUserMail = PM_Vars.current_user.data.user_email,
                is_partial      = args.invoice.partial,
                minimum_partial = parseFloat(args.invoice.minimum_partial),
                paypal_email    = this.getSettings('paypal_mail', '', 'invoice'),
                item_name       = args.invoice.title,
                paymentAmount   = parseFloat(args.amount),
                currency_code   = this.getSettings('currency_code', '', 'invoice'),
                bloginfoName    = PM_Pro_Vars.bloginfo_name,
                sandboxMode     = this.getSettings('sand_box_mode', false, 'invoice'),
                due_amount      = this.dueAmount(this.invoice);


            var request = {
                data: {
                    amount: paymentAmount
                },
                type: 'POST',
                url: self.base_url + 'pm-pro/v2/projects/'+self.project_id+'/invoice/'+args.invoice_id+'/payment-validation', ///with=comments',
                success (res) {
                    if ( sandboxMode || sandboxMode == '1' ) {
                        var paypalUrl = 'https://www.sandbox.paypal.com/webscr/';
                    } else {
                        var paypalUrl = 'https://www.paypal.com/webscr/';
                    }

                    var paypal_args = {
                        'cmd'           : '_xclick',
                        'amount'        : paymentAmount,
                        'business'      : paypal_email,
                        'item_name'     : item_name,
                        'item_number'   : args.invoice.id,
                        'email'         : currentUserMail,
                        'no_shipping'   : '1',
                        'no_note'       : '1',
                        'currency_code' : currency_code,
                        'charset'       : 'UTF-8',
                        'custom'        : '{'+
                            '"invoice_id":'+ args.invoice.id +','+
                            '"user_id":'+ PM_Vars.current_user.data.ID+','+
                            '"project_id":'+ args.invoice.project_id+ ','+
                            '"gateway":' +'"'+ paypalUrl + '"'+
                        '}',
                        'rm'            : '2',
                        'return'        : return_url,
                        'notify_url'    : listener_url,
                        'cbt'           : 'Click here to complete the payment on ' + bloginfoName
                    };
                    self.can_payment_submit = true;
                    var query    = self.getQueryParams(paypal_args);
                    var buildUrl = paypalUrl +'?'+ query;

                    window.location.href = buildUrl;
                },

                error (res) {
                    self.can_submit = true;
                    alert(res.responseJSON.message);
                }
            };

            self.httpRequest(request);




            // if ( paymentAmount <= 0 ) {
            //     alert( "Please insert your payment amount");
            //     return false;
            // }

            // if ( paymentAmount > due_amount ) {
            //     alert("Payment amount should be less than or equal due amount");
            //     return false;
            // }

            // if ( ( is_partial == '1' ) ) {
            //     if(
            //         due_amount >= minimum_partial
            //             &&
            //         paymentAmount <  minimum_partial
            //     ){
            //         alert('Minimum amount greater than ' + minimum_partial);
            //         return false;
            //     } else if (
            //         due_amount < minimum_partial
            //             &&
            //         paymentAmount < due_amount
            //     ) {
            //         alert("Payment should be equal to due amount");
            //         return false;
            //     }

            // } else {

            //     if ( paymentAmount != due_amount ) {
            //         alert( "Payment should equeal due amount", "pm" );
            //     }

            // }


        },

        getDueAmount (invoice) {
            var paidAmount  = this.totalPaid( this.invoice.payments.data );
            var totalAmount = this.invoiceTotal( invoice.entryTasks, invoice.entryNames, invoice.discount );

            var due = (totalAmount - paidAmount);

            return due.toFixed(2);
        },

        capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        checkPermission () {
            if (!this.is_manager()) {
                this.$router.push({
                    name: 'pm_overview',
                    params: {
                        project_id: this.project_id
                    }
                })
            }
        },
        gateWays () {
            var mergedGateWays = [];
            var gateWays = [
                {
                    'name': 'paypal',
                    'label': 'Paypal',
                    'active': false
                }
            ];
            gateWays = pm_apply_filters( 'pm_invoice_gateways', gateWays );
            var saved = this.getSettings('gateWays', gateWays, 'invoice');
            gateWays.forEach( w => {
                var x = saved.findIndex( i => i.name == w.name );
                if (x === -1) {
                    mergedGateWays.push(w);
                } else {
                    mergedGateWays.push(saved[x]);
                }
            });
            return mergedGateWays;
        }
    }
};

